[![License](https://img.shields.io/badge/License-BSD%202--Clause-blue.svg)](https://opensource.org/licenses/BSD-2-Clause)

# EfficientIP Ansible Collection

This is a collection of plugins and modules to interact with [EfficientIP](https://www.efficientip.com/products/) products. 

The version 0.4 is supporting IPAM for ip and networks.

## Documentation

```txt
ansible-doc community.efficientip.solidserver_ipam_ip
ansible-doc community.efficientip.solidserver_ipam_network
```

## Requirements

SOLIDserverRest v2.6.0 and above

## Example Playbooks

Some examples are available on the examples directory of the repo.

## Installation

### using galaxy

* create a `requirements.yml` file like:

```yaml
---
collections:
  - name: https://gitlab.com/efficientip/efficientip-ansible-collection.git
    type: git
```

You can add a `version: %branch%` to test a specific branch of the repo.

* install a virtual environment with ansible

```bash
python3 -m venv env-sds
source env-sds/bin/activate
pip install ansible
```

* install the collection

```bash
ansible-galaxy collection install -r requirements.yml
```

* install dependencies from the requirements.txt file in the repo

```bash
pip install -r %collection%/requirements.txt
```

### from the sources

Using your ansible installation, you can run ```ansible-galaxy collection build``` in the clone of this repo. Once the artefact build, you can install it locally using ```ansible-galaxy collection install community-efficientip-{vers}.tar.gz```.

If you don't want to build it yourself, you can get a prepared artifact from the repo following the commit associated with a tag on the main branch and use the download button from the pipeline attached. Once the artefact is downloaded, you can install it using the collection install command.

## License

BSD-2-Clause
