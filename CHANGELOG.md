# community.efficientip Changelog

## [1.2.0] - 2025-03-07

* add: DNS records

## [1.1.0] - 2025-02-27

* add: network module
* add: token authentication (SDS >= 8.4)
* add: documentation
* update: installation procedure with virtualenv
* fix: usage of the adv part of the SOLIDserverRest lib

## [1.0.2] - 2022-02-11

* fix: module import method
* fix: IP address deletion was not idempotent
* add: Examples for solidserver_ipam_ip usage

## [1.0.1] - 2021-10-27

* fix: PEP8 code compliance
* fix: Option "update" was not working as intended
* add: Example in module solidserver_ipam_ip

## [1.0.0] - 2021-09-06

* Initial release
