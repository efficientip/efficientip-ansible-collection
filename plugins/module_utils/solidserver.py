# -*- coding: utf-8 -*-
#
# This module_utils is PRIVATE and should only be used with EfficientIP module.
# Breaking changes can occur any time.

from __future__ import (absolute_import, division, print_function)

from SOLIDserverRest import SOLIDserverRest
import SOLIDserverRest.adv as sdsadv   # nopep8
from SOLIDserverRest.Exception import SDSInitError, SDSRequestError
from SOLIDserverRest.Exception import SDSAuthError, SDSError
from SOLIDserverRest.Exception import SDSEmptyError, SDSSpaceError
from SOLIDserverRest.Exception import SDSNetworkError, SDSNetworkNotFoundError
from SOLIDserverRest.Exception import SDSDNSError

from http import HTTPStatus
import urllib
import socket
import logging
import ipaddress

__metaclass__ = type
__all__ = ['solidserver', 'ip_version', 'result_ip', 'IPVMAP']

DOCUMENTATION = r'''
---
module: solidserver_utils
short_description: SOLIDserver utils module.
requirements:
  - Python Lib for SOLIDserver: SOLIDserverRest v2.0 and above
description:
  - This module is PRIVATE and should only be used with other EfficientIP modules.
extends_documentation_fragment: nothing
'''


class solidserver:
    sds = None
    timeout = 5

    def __init__(self, provider):
        self.advsds = sdsadv.SDS()

        try:
            ipaddress.IPv4Address(provider['host'])
            self.advsds.set_server_ip(provider['host'])
        except ipaddress.AddressValueError:
            self.advsds.set_server_name(provider['host'])

        _authent = 'basicauth'
        if 'authentication' in provider:
            if provider['authentication'] == 'native':
                _authent = 'native'
            if provider['authentication'] == 'token':
                _authent = 'token'

        if _authent == 'token':
            self.advsds.set_token_creds(keyid=provider['token_id'],
                                        keysecret=provider['token_sec'])
        else:
            self.advsds.set_credentials(user=provider['username'],
                                        pwd=provider['password'])

        self.advsds.set_check_cert('check_tls' not in provider
                                   or provider['check_tls'] is not False)

        if 'proxy' in provider:
            self.advsds.set_proxy_socks(provider['proxy'])

        if 'certificate' in provider:
            self.advsds.set_certificate_file(provider['certificate'])

        if 'timeout' in provider:
            _timeout = int(provider['timeout'])
        else:
            _timeout = 1
        self.advsds.timeout = _timeout

        if 'certificate' in provider:
            _certfile = provider['certificate']
        else:
            _certfile = None

        self.advsds.connect(method=_authent,
                            cert_file_path=_certfile,
                            timeout=_timeout)

        self.sds = self.advsds.sds

    def query(self, query, params=''):
        if self.sds is None:
            return (0, 'SOLIDserver is not defined')
        res = self.sds.query(query, params, self.timeout)
        try:
            json = res.json()
        except ValueError:   # pragma: no cover
            err = ('Status ' + str(res.status_code) +
                   ': ' + HTTPStatus(res.status_code).phrase)
            if res.status_code >= 200 and res.status_code < 300:
                return (res.status_code, err + ' (no data from SOLIDserver)')
            return (res.status_code, err)
        return (res.status_code, json)

    def ip_site_list(self, order=None, limit=None, offset=None):
        p = {}
        if order is not None:
            p['ORDERBY'] = order
        if limit is not None:
            p['limit'] = limit
        if offset is not None:
            p['offset'] = offset
        code, json = self.query('ip_site_list', p)
        return (type(json) == list, code, json)

    def ip_subnet_list(self, ipv, space, terminal=True, order=None, limit=None, offset=None):
        w = []
        w.append("site_name='" + space + "'")
        if terminal:
            w.append("is_terminal='1'")
        p = {'WHERE': ' and '.join(w)}
        if order is not None:
            p['ORDERBY'] = order
        if limit is not None:
            p['limit'] = limit
        if offset is not None:
            p['offset'] = offset
        code, json = self.query(IPVMAP(ipv, 'ip_subnet_list'), p)
        return (type(json) == list, code, json)

    def ip_subnet_get(self, ipv, space, subnet_start, subnet_prefix, terminal=True):
        p = []
        p.append("site_name='" + space + "'")
        if (ipv == 6):
            p.append("start_hostaddr='" + subnet_start + "'")
            p.append("subnet6_prefix=" + str(subnet_prefix))
        else:
            p.append("start_ip_addr='" + str(ip2hex(subnet_start)) + "'")
            p.append("subnet_size=" + str(prefix2size(subnet_prefix)))
        if terminal is True:
            p.append("is_terminal='1'")
        code, json = self.query(IPVMAP(ipv, 'ip_subnet_list'), {
            'WHERE': ' and '.join(p)})
        if code == 204 or isinstance(json, str) or int(json[0]['errno']) > 0:
            return (False, code, json)
        return (True, code, json[0])

    def ip_pool_get(self, ipv, subnet, pool_name):
        p = []
        p.append(IPVMAP(ipv, 'subnet_id') + '=' + subnet)
        p.append(IPVMAP(ipv, 'pool_name') + "='" + pool_name + "'")
        code, json = self.query(IPVMAP(ipv, 'ip_pool_list'), {
            'WHERE': ' and '.join(p)})
        if code == 204 or isinstance(json, str) or int(json[0]['errno']) > 0:
            return (False, code, json)
        return (True, code, json[0])

    def ip_from_name(self, ipv, space, subnet, name, pool=None):
        p = []
        p.append("site_name='" + space + "'")
        p.append(IPVMAP(ipv, 'subnet_id') + '=' + subnet)
        p.append(IPVMAP(ipv, 'name') + "='" + name + "'")
        p.append("type<>'free'")
        if pool is not None:
            p.append(IPVMAP(ipv, 'pool_id') + '=' + pool)
        code, json = self.query(IPVMAP(ipv, 'ip_address_list'), {
            'WHERE': ' and '.join(p)})
        if code == 204 or isinstance(json, str) or int(json[0]['errno']) > 0:
            return (False, code, json)
        return (True, code, json[0])

    def ip_from_addr(self, ipv, space, addr):
        p = []
        p.append("site_name='" + space + "'")
        p.append("hostaddr='" + addr + "'")
        p.append("type<>'free'")
        code, json = self.query(IPVMAP(ipv, 'ip_address_list'), {
            'WHERE': ' and '.join(p)})
        if code == 204 or isinstance(json, str) or int(json[0]['errno']) > 0:
            return (False, code, json)
        return (True, code, json[0])

    def ip_from_id(self, ipv, id):
        code, json = self.query(IPVMAP(ipv, 'ip_address_info'), {
            IPVMAP(ipv, 'ip_id'): id})
        if code == 204 or isinstance(json, str) or int(json[0]['errno']) > 0:
            return (False, code, json)
        return (True, code, json[0])

    def ip_del(self, ipv, ip_id):
        code, json = self.query(IPVMAP(ipv, 'ip_address_delete'), {
            IPVMAP(ipv, 'ip_id'): ip_id})
        if self.json_oid(json):
            return (True, code, json[0])
        return (False, code, json)

    def ip_find(self, ipv, subnet, pool=None, max=15):
        p = {IPVMAP(ipv, 'subnet_id'): subnet, 'max_find': max}
        if pool is not None:
            p[IPVMAP(ipv, 'pool_id')] = pool
        code, json = self.query(IPVMAP(ipv, 'ip_address_find_free'), p)
        return (True, code, json)

    def ip_add(self, ipv, space, addr, name=None, mac=None, class_name=None, class_parameters=None, create=True):
        p = {'site_name': space, 'hostaddr': addr}
        if name is not None:
            p[IPVMAP(ipv, 'name')] = name
        if mac is not None:
            p[IPVMAP(ipv, 'mac_addr')] = mac
        if class_name is not None:
            p[IPVMAP(ipv, 'ip_class_name')] = class_name
        if class_parameters is not None:
            p[IPVMAP(ipv,
                     'ip_class_parameters')] = urllib.parse.urlencode(class_parameters)
        if create is False:
            svc = 'ip_address_update'
        else:
            svc = 'ip_address_create'
        code, json = self.query(IPVMAP(ipv, svc), p)
        if self.json_oid(json):
            return (True, code, json[0])
        return (False, code, json)

    def ip_dynamic(self, ipv, space, subnet, pool=None, name=None, mac=None, class_name=None, class_parameters=None, max_find=15):
        res, code, json = self.ip_find(ipv, subnet, pool, max_find)
        if res is True:
            for addr in json:
                if IPVMAP(ipv, 'hostaddr') in addr:
                    ip_res, ip_code, ip = self.ip_add(ipv,
                                                      space,
                                                      addr[IPVMAP(ipv,
                                                                  'hostaddr')],
                                                      name,
                                                      mac,
                                                      class_name,
                                                      class_parameters)
                    if (ip_res is True):
                        return (ip_res, ip_code, ip)
        res = False
        return (res, code, json)

    def ip_alias_add(self, ipv, ip_id, alias, alias_type='CNAME'):
        p = {
            IPVMAP(ipv, 'ip_id'): ip_id,
            IPVMAP(ipv, 'ip_name'): alias,
            IPVMAP(ipv, 'ip_name_type'): alias_type.upper()
        }
        code, json = self.query(IPVMAP(ipv, 'ip_alias_create'), p)
        if self.json_oid(json):
            return (True, code, json[0])
        return (False, code, json)

    def ip_alias_clear(self, ipv, ip_id):
        code, json = self.query(IPVMAP(ipv, 'ip_alias_list'),
                                {IPVMAP(ipv, 'ip_id'): ip_id}
                                )
        if type(json) == list:
            for alias in json:
                self.query(IPVMAP(ipv, 'ip_alias_delete'),
                           {
                    IPVMAP(ipv, 'ip_name_id'): alias[IPVMAP(ipv, 'ip_name_id')]
                }
                )
            return (True, code, json)
        return (False, code, json)

    def device_add(self, name, space_id, ip, mac=None, nic_number=0):
        device_id = None
        p = []
        p.append("hostdev_name='" + name + "'")
        p.append('hostdev_site_id=' + space_id)
        code, json = self.query('host_device_list', {'WHERE': ' and '.join(p)})
        if type(json) == list and int(json[0]['hostdev_id']) > 0:
            device_id = int(json[0]['hostdev_id'])
        else:
            code, json = self.query('host_device_create',
                                    {
                                        'hostdev_name': name,
                                        'hostdev_site_id': space_id
                                    }
                                    )
            if self.json_oid(json):
                device_id = json[0]['ret_oid']
        if device_id is not None:
            p = {'hostdev_id': device_id}
            p['site_id'] = space_id
            p['hostiface_addr'] = ip
            p['hostiface_type'] = 'interface'
            p['hostiface_name'] = 'nic' + str(nic_number)
            if mac is not None:
                p['hostiface_mac'] = mac
            code, json = self.query('host_iface_create', p)
            return (True, code, json[0])
        return (False, code, json)

    def json_oid(self, json):
        return (type(json) == list and 'ret_oid' in json[0] and int(json[0]['ret_oid']))

    def adv_ip_space_get(self, name=None):
        space = sdsadv.Space(sds=self.advsds, name=name)
        try:
            space.refresh()
        except SDSError:
            return None

        return space

    def ip_network_get(self,
                       space,
                       name,
                       subnet,
                       parent,
                       class_name,
                       class_parameters,
                       is_terminal,
                       is_block):
        # get the space from the configuration
        advspace = self.adv_ip_space_get(name=space)
        if not advspace:
            return (False, None, {'msg': f'space does not exist: {space}'})

        network = sdsadv.Network(sds=self.advsds,
                                 space=advspace,
                                 name=name)

        if subnet:
            net = subnet.split('/')
            if len(net) != 2:
                return (False, None, {'msg': f'Invalid subnet format (expected: address/mask)'})

            ipv = ip_version(net[0])
            if ipv is None:
                return (False, None, {'msg': f'Invalid subnet address (expected: address/mask)'})

            if ipv == 6:
                return (False, None, {'msg': f'IPv6 not yet supported'})

            network.set_address_prefix(net[0], net[1])

        network.set_is_block(is_block)
        network.set_is_terminal(is_terminal)

        try:
            network.refresh()
        except SDSNetworkError:
            return (True, 404, {'advspace': advspace,
                                'advnet': network})

        return (True, 200, {'advspace': advspace,
                            'advnet': network})

    def ip_network_add(self,
                       space,
                       name,
                       subnet,
                       parent,
                       class_name,
                       class_parameters,
                       is_terminal,
                       is_block,
                       net_obj=None):
        """create network based on parameters and context in net_obj (advspace + advnet)"""

        if not net_obj:
            return (False, None, {'msg': f'internal: need to call get_ipam_network first'})

        adv_network = net_obj['advnet']
        adv_space = net_obj['advspace']

        if class_parameters:
            adv_network.set_class_params(class_parameters)

        if class_name:
            adv_network.set_class_name(class_name)

        adv_network.set_is_block(is_block)
        adv_network.set_is_terminal(is_terminal)

        try:
            if adv_network.params['subnet_id']:
                adv_network.update()
                return (True, 200, {'advspace': adv_space,
                                    'advnet': adv_network})
            else:
                # if we have a parent, we need to link back to it
                if parent:
                    res, code, net_obj = self.ip_network_get(space,
                                                             None,
                                                             parent,
                                                             None,
                                                             class_name,
                                                             class_parameters,
                                                             is_terminal,
                                                             is_block)
                    if not res:
                        return (False, None, {'msg': 'parent network not found'})

                    adv_network.set_parent(net_obj['advnet'])

                # now we can create
                try:
                    adv_network.create()
                except SDSNetworkError as e:
                    return (False, 0, {'msg': f'error during network creation {e}'})

                return (True, 201, {'advspace': adv_space,
                                    'advnet': adv_network})
        except SDSNetworkError:
            return (False, 500, {msg: 'error in creating new network',
                                 'advspace': adv_space,
                                 'advnet': adv_network})

    def ip_network_delete(self, adv_net=None):
        """delete an existing network based on a synchronized network object"""

        if not adv_net:
            return (False, None, {'msg': f'internal: need to call get_ipam_network first'})

        network = adv_net

        if network.myid == -1:
            return (False, None, {'msg': f'internal: network object not in sync'})

        try:
            network.delete()
        except SDSNetworkError:
            return (False, 0, {'msg': 'error on network delete'})

        return (True, 200, {})

    # ----------------- DNS SERVER -----------------------
    def dns_server_get(self, dnsserver):
        dns = sdsadv.DNS(name=dnsserver, sds=self.advsds)

        try:
            dns.refresh()
            return (True, None, {'dnsserver': dns})
        except SDSDNSError as e:
            return (False, None, {'msg': str(e)})

    # ----------------- DNS SERVER -----------------------

    # ----------------- DNS ZONE   -----------------------
    def dns_zone_get(self, adv_dnsserver, dnszone):
        dns_zone = sdsadv.DNS_zone(sds=self.advsds, name=dnszone)
        dns_zone.set_dns(adv_dnsserver)

        try:
            dns_zone.refresh()
            return (True, 200, {'dnsserver': adv_dnsserver,
                                'dnszone': dns_zone})

        except SDSError as e:
            return (True, 404, {'dnsserver': adv_dnsserver,
                                'dnszone': dns_zone})
    # ----------------- DNS ZONE   -----------------------

    # ----------------- DNS RR   -----------------------
    def dns_rr_get(self, adv_dnsserver, adv_dnszone,
                   rrtype, rrname, rrvalues):
        dns_rr = sdsadv.DNS_record(sds=self.advsds,
                                   name=rrname)

        if not adv_dnsserver and not adv_dnszone:
            return (False, None, {'msg': 'internal: need zone and/or server'})

        if adv_dnsserver:
            dns_rr.set_dns(adv_dnsserver)

        if adv_dnszone:
            dns_rr.set_zone(adv_dnszone)

        dns_rr.set_type(rrtype)
        dns_rr.set_values(rrvalues)

        try:
            dns_rr.refresh()
            return (True, 200, {'dnsserver': adv_dnsserver,
                                'dnszone': adv_dnszone,
                                'dnsrr': dns_rr})
        except SDSError:
            return (True, 404, {'dnsserver': adv_dnsserver,
                                'dnszone': adv_dnszone,
                                'dnsrr': dns_rr})

    def dns_rr_add(self, adv_dnsserver, adv_dnszone,
                   rrtype, rrname, rrvalues, rrttl,
                   class_parameters,
                   rr_obj):

        if not rr_obj:
            return (False, None, {'msg': f'internal: need to call dns_rr_get first'})

        dns_rr = rr_obj['dnsrr']

        if adv_dnsserver:
            dns_rr.set_dns(adv_dnsserver)

        if adv_dnszone:
            dns_rr.set_zone(adv_dnszone)

        dns_rr.set_type(rrtype)
        dns_rr.set_values(rrvalues)
        dns_rr.set_ttl(int(rrttl))

        if class_parameters:
            dns_rr.set_class_params(class_parameters)

        try:
            if int(dns_rr.myid) > 0:
                dns_rr.update()
                return (True, 200, {'dnsserver': dns_rr.dns_server,
                                    'dnszone': dns_rr.zone,
                                    'dnsrr': dns_rr})

            dns_rr.create()
            return (True, 201, {'dnsserver': adv_dnsserver,
                                'dnszone': adv_dnszone,
                                'dnsrr': dns_rr})
        except SDSError as e:
            return (False, None, {'msg': str(e)})

    def dns_rr_delete(self, adv_dnsserver, adv_dnszone,
                      adv_dnsrr):
        if not adv_dnsrr:
            return (False, None, {'msg': f'internal: need to call dns_rr_get first'})

        try:
            adv_dnsrr.delete()
            return (True, 200, {'dnsserver': adv_dnsserver,
                                'dnszone': adv_dnszone,
                                'dnsrr': adv_dnsrr})
        except SDSDNSError as e:
            return (False, None, {'msg': str(e)})
    # ----------------- DNS RR   -----------------------


def ip_version(ip):
    try:
        socket.inet_pton(socket.AF_INET, ip)
        return 4
    except socket.error:
        try:
            socket.inet_pton(socket.AF_INET6, ip)
            return 6
        except socket.error:
            return None


def ip2hex(ip):
    ip = ip.split('.')
    if len(ip) != 4:
        return '00000000'
    return ''.join('{:02x}'.format(int(i)) for i in ip)


def prefix2size(prefix):
    pfx = int(prefix)
    if pfx < 0 or pfx > 32:
        return 0
    return 1 << (32 - pfx)


def size2prefix(size):
    pfx = 32
    while (pfx and not (int(size) & (1 << (32 - pfx)))):
        pfx -= 1
    return pfx


def result_ip(result, ip):
    if 'mac_addr' in ip and ip['mac_addr'][0:3] == 'EIP':
        ip.pop('mac_addr')
    if 'address' in ip:
        result['address'] = ip['hostaddr']
    result['ip'] = ip
    return result


IPV6MAP = {
    'hostaddr': 'hostaddr6',
    'ip_address_create': 'ip_address6_create',
    'ip_address_delete': 'ip_address6_delete',
    'ip_address_find_free': 'ip_address6_find_free',
    'ip_address_info': 'ip_address6_info',
    'ip_address_list': 'ip_address6_list',
    'ip_address_update': 'ip_address6_update',
    'ip_alias_create': 'ip_alias6_create',
    'ip_alias_delete': 'ip_alias6_delete',
    'ip_alias_list': 'ip_alias6_list',
    'ip_class_name': 'ip6_class_name',
    'ip_class_parameters': 'ip6_class_parameters',
    'ip_id': 'ip6_id',
    'ip_name': 'ip6_name',
    'ip_name_type': 'ip6_name_type',
    'ip_name_id': 'ip6_name_id',
    'ip_pool_list': 'ip_pool6_list',
    'ip_subnet_list': 'ip_subnet6_list',
    'mac_addr': 'ip6_mac_addr',
    'name': 'ip6_name',
    'pool_id': 'pool6_id',
    'pool_name': 'pool6_name',
    'subnet_id': 'subnet6_id',
}


def IPVMAP(ipv, svc):
    if ipv == 6:
        return IPV6MAP[svc]
    return svc


def dbg(msg):
    logging.warning(msg)
