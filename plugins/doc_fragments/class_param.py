#

from __future__ import (absolute_import, division, print_function)
__metaclass__ = type


class ModuleDocFragment(object):
    DOCUMENTATION = """
options:
  class_name:
    description:
      - SOLIDserver class name
    type: str
    required: false

  class_parameters:
    description:
      - SOLIDserver class parameters, as a dictionary with key and value
    type: dict
    required: false
"""
