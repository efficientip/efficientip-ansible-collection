#

from __future__ import (absolute_import, division, print_function)
__metaclass__ = type


class ModuleDocFragment(object):
    DOCUMENTATION = """
options:
  state:
    description:
      - Does this object needs to be present or absent from the SOLIDserver
    type: str
    required: false
    default: present
    choices:
      - present
      - absent
"""
