#

from __future__ import (absolute_import, division, print_function)
__metaclass__ = type


class ModuleDocFragment(object):
    DOCUMENTATION = """
options:
  space:
    description:
      - SOLIDserver IPAM Space name
    type: str
    required: true
"""
