#

from __future__ import (absolute_import, division, print_function)
__metaclass__ = type


class ModuleDocFragment(object):
    DOCUMENTATION = """
options:
  provider:
    description:
      - SOLIDserver host to connect to
    type: dict
    suboptions:
      host:
        description:
          - the SOLIDserver manager host to connect to, can be either an 
          - IP address or a FQDN
        type: str
        default: sds.lab

      authentication: 
        description:
          - the authentication method to use, it can be either 'native' to use
          - specific headers or 'basic' for login/password bearer header, or 
          - 'token'
        choices: [ native, basic, token ]
        type: str
        default: basic

      username:
        description:
          - the user name as it exists in the SOLIDserver, the permissions will
          - be the one of this user
          - this is required for basic and native authentications
        type: str
        
      password: 
        description:
          - the associated password for this user (requires username to be set)
          - this is required for basic and native authentications
        type: str

      token_id:
        description:
          - when using token authentication, here is the public part of the
          - token, visible in the administration page on the SOLIDserver
        type: str

      token_sec:
        description:
          - when using token authentication, here is the private part of the
          - token, presented at the creation time
        type: str

      check_tls:
        description:
          - since the API connection uses TLS, specify here if the certificate 
          - needs to be validated
        type: boolean
        default: true

      timeout:
        description:
          - the API call timeout expressed in second, both for connection and 
          - transactions, if your request can take long, this needs to be
          - increased
        type: int
        default: 5

      proxy:
        description:
          - if using an HTTP proxy to access the SOLIDserver, contains the host 
          - and port information, like '192.168.1.1:9001'
        type: str

      certificate:
        description:
          - if the SOLIDserver runs a private certificate (signed by an internal 
          - authority), you set here the file name containing the public part
          - of the certificate
        type: str
"""
