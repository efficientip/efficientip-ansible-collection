#

from __future__ import (absolute_import, division, print_function)
__metaclass__ = type


class ModuleDocFragment(object):
    DOCUMENTATION = """
options:
  update:
    description:
      - Update SOLIDserver object if it already exists
    type: bool
    required: false
    default: true
"""
