from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

DOCUMENTATION = '''
lookup: solidserver_ipam_ip_find_free_address
short_description: Find free IP addresses in SOLIDserver IPAM.
description:
  - This module produces a list of free IP addresses from SOLIDserver IPAM.
options:
  host:
    description: 'SOLIDserver hostname or IP address'
  authentication:
    description: 'Authentication mode (basic|native)'
  username:
    description: 'Authentication username'
  password:
    description: 'Authentication password'
  check_tls:
    description: 'Check SSL certificate (default: True)'
  space:
    description: 'IPAM space'
  subnet:
    description: 'IPAM subnet'
  max:
    description: 'Maximum IP addresses in result (default: 1)'
'''

EXAMPLES = """
vars:
  spaces: "{{ lookup('solidserver_ipam_ip_find_free_address', host='192.168.1.1', authentication='basic',
                      username='ipmadmin', password='password', space='Local', subnet='192.168.1.0/24', wantlist=True) }}"
"""

RETURN = """
_raw:
  description: comma-separated list of IPs
"""


import logging
from ansible.errors import AnsibleError
from ansible.plugins.lookup import LookupBase
from ansible_collections.community.efficientip.plugins.module_utils.solidserver import solidserver, IPVMAP, ip_version


class LookupModule(LookupBase):
    def run(self, terms, variables, **kwargs):
        if 'host' not in kwargs:
            raise AnsibleError("Missing SOLIDserver host")
        if 'username' not in kwargs:
            raise AnsibleError("Missing SOLIDserver username")
        if 'password' not in kwargs:
            raise AnsibleError("Missing SOLIDserver password")
        if 'space' not in kwargs:
            raise AnsibleError("Missing SOLIDserver IPAM space")
        if 'subnet' not in kwargs:
            raise AnsibleError("Missing SOLIDserver IPAM subnet")
        net = kwargs['subnet'].split('/')
        if len(net) != 2:
            raise AnsibleError('Invalid subnet format (expected: address/mask)')
        ipv = ip_version(net[0])
        if ipv is None:
            raise AnsibleError('Invalid subnet address (expected: address/mask)')
        max = 1
        if 'max' in kwargs:
            max = kwargs['max']
        provider = {'host': kwargs['host'], 'username': kwargs['username'], 'password': kwargs['password']}
        if 'authentication' in kwargs and kwargs['authentication'] == 'native':
            provider['authentication'] = 'native'
        provider['check_tls'] = 'check_tls' not in kwargs or kwargs['check_tls'] is not False
        sds = solidserver(provider)
        res, code, net = sds.ip_subnet_get(ipv, kwargs['space'], net[0], net[1], True)
        if res is False:
            if code == 204:
                raise AnsibleError('Requested subnet ' + kwargs['subnet'] + ' not found in space ' + kwargs['space'])
            raise AnsibleError('Failed to retrieve subnet ' + kwargs['subnet'] + ' from IPAM')
        subnet_id = net[IPVMAP(ipv, 'subnet_id')]
        res, code, json = sds.ip_find(ipv, subnet_id, None, max)
        if res is False:
            raise AnsibleError("Failed to retrieve IP(s) from IPAM")
        return [ip[IPVMAP(ipv, 'hostaddr')] for ip in json]
