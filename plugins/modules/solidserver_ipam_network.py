#!/usr/bin/python
# -*- coding: utf-8 -*-
##########################################################
'''
Ansible module for EfficientIP SOLIDserver
IPAM Network manipulations
'''

from __future__ import (absolute_import, division, print_function)
from ansible_collections.community.efficientip.plugins.module_utils.solidserver import *
from ansible.module_utils.basic import AnsibleModule
import re
import logging
import uuid

__metaclass__ = type

ANSIBLE_METADATA = {
    'metadata_version': '1.1',
    'status': ['preview'],
    'supported_by': 'EfficientIP community',
}

DOCUMENTATION = """
module: solidserver_ipam_network

short_description: SOLIDserver IPAM network manipulation module.

author:
  - MBE
  - ACH

description:
  - This module permits the manipulation of IP addresses from SOLIDserver IPAM. It can
  - create, modify and delete networks, either block, non terminal network and terminal
  - network. It supports class parameters and class assiciation and parenting positionning

version_added: 1.1.0

extends_documentation_fragment:
  - community.efficientip.provider
  - community.efficientip.ipam_space
  - community.efficientip.class_param
  - community.efficientip.state
  - community.efficientip.update

options:
  name:
    description:
      - SOLIDserver IPAM Space name
    type: str
    required: false
    default: uuid4

  subnet:
    description:
      - SOLIDserver IPAM subnet address with prefix
    type: str
    required: false

  parent:
    description:
      - SOLIDserver IPAM subnet address with prefix in which the subnet will be created/updated
    type: str
    required: false

  is_terminal:
    description:
      - the network is terminal
    type: bool
    required: false
    default: false

  is_block:
    description:
      - the network is a block
    type: bool
    required: false
    default: false
"""


EXAMPLES = """
  - name: Create a network
    community.efficientip.solidserver_ipam_network:
      space: Local
      name: testnet
      subnet: 192.168.96.0/22
      parent: 192.168.96.0/20
      class_name: netansible
      class_parameters:
        color: blue
        env: sandbox
      update: true
      state: present
      provider: "{{ solidserver }}"    
"""

RETURN = """
  sds_network_address:
    description: network address
    returned: when success and state present
    type: str

  sds_network_prefix:
    description: prefix size
    type: int
    returned: when success and state present

  sds_network_used_percent:
    description: 
      - occupation of the network in either addresses if terminal 
      - or subnet if not
    type: int
    returned: when success and state present

  sds_network_id:
    description: technical object id in the SOLIDserver, for internal use
    type: int
    returned: when success

  sds_network_name:
    description: name of the network
    type: str
    returned: when success and state present
"""


def main():
    """ main function for ansible module """
    module_args = dict(
        provider=dict(type='dict', required=True),
        space=dict(type='str', required=True),
        subnet=dict(type='str', required=False, default=None),
        parent=dict(type='str', required=False, default=None),
        class_name=dict(type='str', required=False, default=None),
        class_parameters=dict(type='dict', required=False, default=None),
        update=dict(type='bool', required=False, default=True),
        state=dict(type='str', required=True),
        is_terminal=dict(type='bool', required=False, default=False),
        is_block=dict(type='bool', required=False, default=False),
        name=dict(type='str', required=False, default=None),
    )

    module = AnsibleModule(argument_spec=module_args, supports_check_mode=True)
    result = dict(changed=False)
    result['changed'] = False
    result['created'] = False

    # get variables from playbook
    provider = module.params['provider']
    space = module.params['space']
    subnet = module.params['subnet']
    parent = module.params['parent']
    class_name = module.params['class_name']
    class_parameters = module.params['class_parameters']
    update = module.params['update']
    state = module.params['state']
    is_terminal = module.params['is_terminal']
    is_block = module.params['is_block']
    name = module.params['name']

    # if we need to create a network, it should have a name
    if not name and state == "present":
        name = str(uuid.uuid4())

    result['call_params'] = module.params
    del result['call_params']['provider']

    if not space:
        module.fail_json(msg='space should be provided',
                         **result)

    if not subnet and not name:
        module.fail_json(msg='subnet or name should be provided',
                         **result)

    if module.check_mode:
        module.exit_json(**result)

    # init the solidserver connection
    sds = solidserver(provider)

    # get existing network
    res, code, net_obj = sds.ip_network_get(space,
                                            name,
                                            subnet,
                                            parent,
                                            class_name,
                                            class_parameters,
                                            is_terminal,
                                            is_block)

    if not res:
        module.fail_json(msg=net_obj['msg'], **result)

    _network_str = str(net_obj['advnet'])

    # network is not found
    if code == 404:
        if state == "present":
            # we need to create the network
            res, code, net_obj = sds.ip_network_add(space,
                                                    name,
                                                    subnet,
                                                    parent,
                                                    class_name,
                                                    class_parameters,
                                                    is_terminal,
                                                    is_block,
                                                    net_obj)

            if not res:
                module.fail_json(msg=net_obj['msg'], **result)

            result['created'] = True
            result['changed'] = True

            # add return data
            result['sds_network_id'] = net_obj['advnet'].myid
            result['sds_network_address'] = net_obj['advnet'].subnet_addr
            result['sds_network_prefix'] = net_obj['advnet'].subnet_prefix
            result['sds_network_name'] = net_obj['advnet'].name

            if net_obj['advnet'].is_terminal:
                result['sds_network_used_percent'] = round(float(
                    net_obj['advnet'].params['subnet_ip_used_percent']))
            else:
                result['sds_network_used_percent'] = round(float(
                    net_obj['advnet'].params['subnet_allocated_percent']))

            module.exit_json(**result)

        elif state == "absent":
            result['created'] = False
            result['changed'] = False
            module.exit_json(**result)

    if state == "present":
        # change something
        if update:
            res, code, net_obj = sds.ip_network_add(space,
                                                    name,
                                                    subnet,
                                                    parent,
                                                    class_name,
                                                    class_parameters,
                                                    is_terminal,
                                                    is_block,
                                                    net_obj)

            if not res:
                module.fail_json(msg=net_obj['msg'], **result)

            # do we have an update?
            if code == 200:
                if _network_str != str(net_obj['advnet']):
                    result['changed'] = True

            # do we have a create?
            elif code == 201:
                result['changed'] = True
                result['created'] = True

        result['sds_network_id'] = net_obj['advnet'].myid
        result['sds_network_address'] = net_obj['advnet'].subnet_addr
        result['sds_network_prefix'] = net_obj['advnet'].subnet_prefix
        result['sds_network_name'] = net_obj['advnet'].name

        if net_obj['advnet'].is_terminal:
            result['sds_network_used_percent'] = round(float(
                net_obj['advnet'].params['subnet_ip_used_percent']))
        else:
            result['sds_network_used_percent'] = round(float(
                net_obj['advnet'].params['subnet_allocated_percent']))

    elif state == "absent":
        # we need to delete it
        res, code, net_obj = sds.ip_network_delete(net_obj['advnet'])

        if not res:
            module.fail_json(msg=net_obj['msg'], **result)

        result['created'] = False
        result['changed'] = True

    module.exit_json(**result)


main()
