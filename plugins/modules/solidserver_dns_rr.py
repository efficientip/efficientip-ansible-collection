#!/usr/bin/python
##########################################################
'''
Ansible module for EfficientIP SOLIDserver
DNS records manipulations
'''

from __future__ import (absolute_import, division, print_function)
from ansible_collections.community.efficientip.plugins.module_utils.solidserver import *
from ansible.module_utils.basic import AnsibleModule
import re
import logging
import uuid

__metaclass__ = type

ANSIBLE_METADATA = {
    'metadata_version': '1.1',
    'status': ['preview'],
    'supported_by': 'EfficientIP community',
}

DOCUMENTATION = """
module: solidserver_dns_rr

short_description: SOLIDserver DNS record manipulation module.

author:
  - ACH

description:
  - This module permits the manipulation of DNS records from SOLIDserver DNS module.
  - It can create, modify and delete records

version_added: 1.2.0

extends_documentation_fragment:
  - community.efficientip.provider
  - community.efficientip.class_param
  - community.efficientip.state
  - community.efficientip.update

options:
  name:
    description:
      - DNS record name
    type: str
    required: true

  dnsserver:
    description:
      - SOLIDserver DNS server name, smart or server
    type: str
    required: true

  dnsview:
    description:
      - SOLIDserver DNS view within server
    type: str
    required: false

  dnszone:
    description:
      - SOLIDserver DNS zone name, required if name is not an FQDN
    type: str
    required: false

  rrtype:
    description:
      - the record type
    type: str
    choices:
      - TXT
      - A / AAAA
      - CNAME
      - MX
      - NS
      - SRV
    required: true

  rrvalues:
    description:
      - the values associated to the record, depends on the type
    type: list
    elements: str
    required: true

  rrttl:
    description: TTL value in second
    type: int
    default: 3600
    required: false

"""


EXAMPLES = """

"""

RETURN = """
"""


def main():
    """ main function for ansible module """
    module_args = dict(
        provider=dict(type='dict', required=True),
        name=dict(type='str', required=True, default=None),
        dnsserver=dict(type='str', required=True),
        dnsview=dict(type='str', required=False, default=None),
        dnszone=dict(type='str', required=False, default=None),
        rrtype=dict(type='str', required=True),
        rrvalues=dict(type='list', required=True),
        class_name=dict(type='str', required=False, default=None),
        class_parameters=dict(type='dict', required=False, default=None),
        update=dict(type='bool', required=False, default=True),
        state=dict(type='str', required=True),
        rrttl=dict(type='int', required=False, default=3600),

    )

    module = AnsibleModule(argument_spec=module_args, supports_check_mode=True)
    result = dict(changed=False)
    result['changed'] = False
    result['created'] = False

    # get variables from playbook
    provider = module.params['provider']
    name = module.params['name']
    dnsserver = module.params['dnsserver']
    dnsview = module.params['dnsview']
    dnszone = module.params['dnszone']
    class_name = module.params['class_name']
    class_parameters = module.params['class_parameters']
    update = module.params['update']
    state = module.params['state']
    rrtype = module.params['rrtype']
    rrvalues = module.params['rrvalues']
    rrttl = module.params['rrttl']

    result['call_params'] = module.params
    del result['call_params']['provider']

    # checks
    if module.check_mode:
        module.exit_json(**result)

    # init the solidserver connection
    sds = solidserver(provider)

    # find server
    res, code, rr_obj = sds.dns_server_get(dnsserver)
    if not res:
        module.fail_json(msg=rr_obj['msg'], **result)

    # find zone
    if dnszone:
        # if the name is not fully qualified, build one with the zone name
        if not name.endswith(dnszone):
            name = name+f'.{dnszone}'
        res, code, rr_obj = sds.dns_zone_get(rr_obj['dnsserver'], dnszone)

        if not res:
            module.fail_json(msg=rr_obj['msg'], **result)

        if code == 404:
            rr_obj['dnszone'] = None
    else:
        rr_obj['dnszone'] = None

    # find rr
    res, code, rr_obj = sds.dns_rr_get(adv_dnsserver=rr_obj['dnsserver'],
                                       adv_dnszone=rr_obj['dnszone'],
                                       rrtype=rrtype,
                                       rrname=name,
                                       rrvalues=rrvalues)
    # general error
    if not res:
        module.fail_json(msg=net_obj['msg'], **result)

    # do we have this record already
    if code == 404:
        if state == "present":  # present and not found
            # create rr
            res, code, rr_obj = sds.dns_rr_add(adv_dnsserver=rr_obj['dnsserver'],
                                               adv_dnszone=rr_obj['dnszone'],
                                               rrtype=rrtype,
                                               rrname=name,
                                               rrvalues=rrvalues,
                                               rrttl=rrttl,
                                               class_parameters=class_parameters,
                                               rr_obj=rr_obj)

            if not res:
                module.fail_json(msg=rr_obj['msg'], **result)

            result['created'] = True
            result['changed'] = True

            result['sds_dns_rr_id'] = rr_obj['dnsrr'].myid
            result['sds_dns_rr_server'] = rr_obj['dnsrr'].dns_server.name
            result['sds_dns_rr_zone'] = rr_obj['dnsrr'].zone.name

            if 'rr_glue' in rr_obj['dnsrr'].params:
                result['sds_dns_rr_glue'] = rr_obj['dnsrr'].params['rr_glue']

        else:  # absent and not found
            result['created'] = False
            result['changed'] = False
            module.exit_json(**result)
    else:
        if state == "present":  # present and found
            if update:
                # update rr
                rrstr_orig = str(rr_obj['dnsrr'])

                res, code, rr_obj = sds.dns_rr_add(adv_dnsserver=rr_obj['dnsserver'],
                                                   adv_dnszone=rr_obj['dnszone'],
                                                   rrtype=rrtype,
                                                   rrname=name,
                                                   rrvalues=rrvalues,
                                                   rrttl=rrttl,
                                                   class_parameters=class_parameters,
                                                   rr_obj=rr_obj)

                rrstr = str(rr_obj['dnsrr'])

                if rrstr != rrstr_orig:
                    result['created'] = True
                    result['changed'] = True

                result['sds_dns_rr_id'] = rr_obj['dnsrr'].myid
                result['sds_dns_rr_server'] = rr_obj['dnsrr'].dns_server.name
                result['sds_dns_rr_zone'] = rr_obj['dnsrr'].zone.name

                if 'rr_glue' in rr_obj['dnsrr'].params:
                    result['sds_dns_rr_glue'] = rr_obj['dnsrr'].params['rr_glue']
        else:
            res, code, rr_obj = sds.dns_rr_delete(adv_dnsserver=rr_obj['dnsserver'],
                                                  adv_dnszone=rr_obj['dnszone'],
                                                  adv_dnsrr=rr_obj['dnsrr'])
            if res and code == 200:
                result['changed'] = True

    module.exit_json(**result)


main()
