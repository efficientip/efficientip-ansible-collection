#!/usr/bin/python
# -*- coding: utf-8 -*-
##########################################################
'''
Ansible module for EfficientIP SOLIDserver
IPAM IP Address manipulations
'''

from __future__ import (absolute_import, division, print_function)
from ansible_collections.community.efficientip.plugins.module_utils.solidserver import *
from ansible.module_utils.basic import AnsibleModule
import re
import logging

__metaclass__ = type

ANSIBLE_METADATA = {
    "metadata_version": "1.1",
    'status': ['preview'],
    'supported_by': 'EfficientIP community',
    'version': '0.3'
}

DOCUMENTATION = """
module: solidserver_ipam_ip

short_description: SOLIDserver IPAM IP address manipulation module.

author:
  - MBE
  - ACH

description:
  - This module permits the manipulation of IP addresses from SOLIDserver IPAM

version_added: 0.1

extends_documentation_fragment:
  - community.efficientip.provider
  - community.efficientip.ipam_space
  - community.efficientip.class_param
  - community.efficientip.state
  - community.efficientip.update

options:
  subnet:
    description:
      - SOLIDserver IPAM terminal subnet address with prefix
    type: str
    required: true
  hostname:
    description:
      - SOLIDserver IPAM IP address name
    type: str
    required: true
  ip:
    description:
      - SOLIDserver IPAM IP address or "dynamic" keyword
    type: str
    required: true
  pool_name:
    description:
      - SOLIDserver IPAM pool name (must be within subnet)
    type: str
    required: false
  mac:
    description:
      - MAC address
    type: str
    required: false

  aliases:
    description:
      - IP address aliases
    type: dict
    required: false

"""

EXAMPLES = """
- name: Example solidserver_ipam_ip playbook
  hosts: servers
  vars:
    solidserver:
      host: <solidserver.domain>
      authentication: basic
      username: <username>
      password: <password>
      check_tls: true
      timeout: 5
  connection: local
  tasks:
  - name: add IP address 192.168.1.250 to the IPAM
    community.efficientip.solidserver_ipam_ip:
      space: Local
      subnet: 192.168.1.0/24
      hostname: ipv4_static
      ip: 192.168.1.250
      mac: 02:03:04:05:06:07
      aliases:
        alias_a1: a
        alias_a2: a
        alias_cname1: cname
        alias_cname2: cname
      class_name: ansible
      class_parameters:
        param1: value1
        param2: value2
      update: true
      state: present
      provider: "{{ solidserver }}"
    register: testout
  - name: dump test output
    debug:
      msg: '{{ testout }}'
"""

RETURN = """
  address:
    description: Allocated IP address
    returned: when success
    type: str
  ip:
    description: SOLIDserver IPAM IP address properties
    returned: when success
    type: dict
"""


def main():
    """ main function for ansible module """
    module_args = dict(
        provider=dict(type='dict', required=True),
        space=dict(type='str', required=True),
        subnet=dict(type='str', required=True),
        pool_name=dict(type='str', required=False, default=None),
        hostname=dict(type='str', required=True),
        ip=dict(type='str', required=True),
        mac=dict(type='str', required=False, default=None),
        aliases=dict(type='dict', required=False, default=None),
        class_name=dict(type='str', required=False, default=None),
        class_parameters=dict(type='dict', required=False, default=None),
        update=dict(type='bool', required=False, default=True),
        state=dict(type='str', required=False, default=True)
    )

    module = AnsibleModule(argument_spec=module_args, supports_check_mode=True)
    result = dict(changed=False)

    # get variables from playbook
    provider = module.params['provider']
    space = module.params['space']
    subnet = module.params['subnet']
    pool_name = module.params['pool_name']
    hostname = module.params['hostname']
    ip = module.params['ip']
    mac = module.params['mac']
    class_name = module.params['class_name']
    class_parameters = module.params['class_parameters']
    update = module.params['update']
    state = module.params['state']

    result['call_params'] = module.params
    del result['call_params']['provider']

    net = subnet.split('/')
    if len(net) != 2:
        module.fail_json(msg='Invalid subnet format (expected: address/mask)',
                         **result)
    if state != 'present' and state != 'absent':
        module.fail_json(msg='Invalid state (present|absent)', **result)
    ipv = ip_version(net[0])
    if ipv is None:
        module.fail_json(msg='Invalid subnet address (expected: address/mask)',
                         **result)
    if ip != 'dynamic' and ip_version(ip) != ipv:
        module.fail_json(msg=f'Invalid IP address ({ip}) for subnet {subnet}',
                         **result)
    if module.params['state'] == 'present':
        if mac is not None:
            if not re.match('^[0-9a-f]{2}(:[0-9a-f]{2}){5}$', mac.lower()):
                module.fail_json(msg=f'Invalid MAC address format ({mac})',
                                 **result)
        aliases = module.params['aliases']
        if aliases is not None:
            for a in aliases:
                if aliases[a] != 'cname':
                    if ipv == 4 and aliases[a] != 'a':
                        module.fail_json(msg=f'Invalid alias type ({aliases[a]}) for IPv4',
                                         **result)
                    elif ipv == 6 and aliases[a] != 'aaaa':
                        module.fail_json(msg=f'Invalid alias type ({aliases[a]}) for IPv6',
                                         **result)
    if module.check_mode:
        module.exit_json(**result)

    sds = solidserver(provider)

    res, code, net = sds.ip_subnet_get(ipv, space, net[0], net[1], True)
    if res is False:
        if code == 204:  # not found
            if state == 'present':
                module.fail_json(msg=f'Requested subnet {subnet} not found in space {space}',
                                 **result)
            else:
                process_result(module, sds, True, code, ipv, ip, result)
                module.exit_json(**result)

        module.fail_json(msg=net, **result)
    subnet_id = net[IPVMAP(ipv, 'subnet_id')]

    # find pool in network
    if pool_name is None:
        pool_id = None
    else:
        res, code, pool = sds.ip_pool_get(ipv, subnet_id, pool_name)
        if res is False:
            if code == 204:
                if state == 'present':
                    module.fail_json(msg=f'Requested pool {pool_name} not found in space {space}',
                                     **result)
                else:
                    process_result(module, sds, True, code, ipv, addr, result)
                    module.exit_json(**result)

            module.fail_json(msg=net, **result)
        pool_id = pool[IPVMAP(ipv, 'pool_id')]

    # Check if IP already exists
    if ip == 'dynamic':
        res, code, addr = sds.ip_from_name(ipv,
                                           space,
                                           subnet_id,
                                           hostname,
                                           pool_id)
    else:
        res, code, addr = sds.ip_from_addr(ipv, space, ip)

    if state == 'absent':
        if ip == 'dynamic':
            if res is False:
                if code == 204:
                    process_result(module, sds, True, code, ipv, addr, result)
                # non-idempotent alternative:
                # module.fail_json(msg='IP address name ' + hostname + ' not found in subnet ' + subnet, **result)
                module.exit_json(**result)
        else:
            if res is False:
                # non-idempotent alternative:
                # module.fail_json(msg='IP address ' + ip + ' not found in space ' + space, **result)
                module.exit_json(**result)
        res, code, addr = sds.ip_del(ipv, addr[IPVMAP(ipv, 'ip_id')])
    else:
        if ip == 'dynamic':
            if res is True:
                if update:
                    res, code, addr = sds.ip_add(ipv,
                                                 space,
                                                 addr['hostaddr'],
                                                 hostname,
                                                 mac,
                                                 class_name,
                                                 class_parameters,
                                                 False)
                process_result(module, sds, res, code, ipv, addr, result)
            # Create a new IP
            res, code, addr = sds.ip_dynamic(ipv,
                                             space,
                                             subnet_id,
                                             pool_id,
                                             hostname,
                                             mac,
                                             class_name,
                                             class_parameters)
        else:
            if res is True and addr[IPVMAP(ipv, 'subnet_id')] == subnet_id:
                if addr[IPVMAP(ipv, 'name')] == hostname:
                    if update:
                        res, code, addr = sds.ip_add(ipv,
                                                     space,
                                                     ip,
                                                     hostname,
                                                     mac,
                                                     class_name,
                                                     class_parameters,
                                                     False)
                    process_result(module, sds, res, code, ipv, addr, result)
                module.fail_json(msg=f'IP address {ip} is already allocated for host "{addr["name"]}"',
                                 **result)
            # Create a new IP
            res, code, addr = sds.ip_add(ipv,
                                         space,
                                         ip,
                                         hostname,
                                         mac,
                                         class_name,
                                         class_parameters)
    process_result(module, sds, res, code, ipv, addr, result)


def process_result(module, sds, res, code, ipv, addr, result):
    result['changed'] = False
    result['changed'] = False

    if res is False:
        module.fail_json(msg=addr, **result)
    if code == 200:
        result['changed'] = True
    if code == 201:
        result['changed'] = True
        result['created'] = True
    if module.params['state'] == 'present' and 'ret_oid' in addr:
        res, code, addr = sds.ip_from_id(ipv, addr['ret_oid'])
        ip_id = addr[IPVMAP(ipv, 'ip_id')]
        ip_name = addr[IPVMAP(ipv, 'name')]
        # Aliases
        if module.params['update']:
            sds.ip_alias_clear(ipv, ip_id)
        aliases = module.params['aliases']
        if aliases is not None:
            result['aliases'] = []
            for a in aliases:
                res, code, alias = sds.ip_alias_add(ipv, ip_id, a, aliases[a])
                if res is True:
                    result['aliases'].append(alias)
    result_ip(result, addr)
    module.exit_json(**result)


main()
